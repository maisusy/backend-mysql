module.exports = (sequelize,type)=>{
    return sequelize.define("productos",{
        nom : type.STRING,
        precio : type.INTEGER
    },
    {
        sequelize,
        timestamps: false
    }
    )
}