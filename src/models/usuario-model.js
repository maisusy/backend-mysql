module.exports = (sequelize,type)=>{
    return sequelize.define("usuarios",{
        nom : type.STRING,
        edad : type.INTEGER
    },
    {
        sequelize,
        timestamps:false
    }
    )
}