const router = require("express").Router()
const {Frutas} = require("../../../database/sync")

router.get("/",async(req,res)=>{
    const frutas = await Frutas.findAll()
    res.json(frutas)
})

module.exports = router