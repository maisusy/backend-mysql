require("dotenv").config()

const {Sequelize} = require("sequelize")
const {database} = require("../config")

console.log(database)

const databaseAcamica =  new Sequelize (database.database,
    database.username,
    database.password,{
    host : database.host,
    port : database.port,
    dialect : database.dialect
})


async function validar_conexion(){
    try{
        await databaseAcamica.authenticate();
        console.log("conexion ha sido establecida")
    }catch(err){
        console.log(err)
    }
}

validar_conexion()

module.exports = {
    databaseAcamica,
    Sequelize
}