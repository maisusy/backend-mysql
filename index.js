const express = require("express")

const app = express()

app.use(express.json())

app.use(express.urlencoded({extended:true}))


require("./src/database/db")

require("./src/database/sync")

//apiRouter = require("./src/routers/api.js")


//app.use("/api",apiRouter)

app.listen(3000,()=>{
    console.log("escuchando en 3000")
})